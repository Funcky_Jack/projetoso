/*
 * Ficheiro: servidor.c
 * Autor: José Carlos Gaspar Loureiro
 * nº: 13745
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <pthread.h>

#define NOME_FICH_SOCKET "FICH_SOCKET"  // Constante que vai definir o nome do ficherio de socket

int clientes_ligados = 0;               // Variável que vai conter o número de clientes conectados

// Funcao para escrever no socket (interpretado como um ficheiro)
void escrever_socket(int socketfd, char data[]){

    FILE *fich;             // Declarado variável do tipo ficheiro
    char linha[200]="";     // Espaço reservado para leitura de uma linha
    char linhas[800]="";    // Espaço reservado para receber a concatenação de cada linha

    // Abre ficheiro para leitura
    fich = fopen("noticias.txt", "r");

    // Percorre o ficheiro linha a linha equanto houver dados
    while(fgets(linha, sizeof(linha), fich) != NULL){

        // Se existe uma ocorrência que contenha a data na linha
        if(strstr(linha, data)){
            strcat(linhas, linha);      // Concatena a linha lida para a variável linhas
        }
    }

    // Fecha o ficheiro
    fclose(fich);

    /* Se nenhuma conteudo for devolvido na pesquisa,
       informa o utilizador que não existem registos para a data
       que introduziu */
    if(strlen(linhas) == 0){
        strcpy(linhas, "Nao existem registos para a data que inseriu...");
    }

    // Escreve no socket o resultado
    write(socketfd, linhas, sizeof(linhas));
}

/* funcao para ler no socket (interpretado como um ficheiro)
   enquanto a mensagem do cliente nao for "end" ou vazia, o servidor vai
   lendo o que esta escrito no socket */
void *ler_socket(void* cl_socket){

    // Descriptor do socket
    int cliente_socket = *(int *) cl_socket;

    // Ciclo infinito
    while(1){

        // Variável que guarda o tamanho da mensagem
        int tamanho;

        /* Se a leitura for vazia fechamos a ligacao, ou seja, retornamos
           ao main */
        if(read(cliente_socket, &tamanho, sizeof(tamanho)) == 0)
            break;

        // Reservar espaco em memoria para guardar a mensagem
        char *mensagem = (char*) malloc(sizeof(mensagem));

        // Ler a mensagem do cliente
        read(cliente_socket, mensagem, tamanho);

        /* Se a mensagem do cliente for 'end' fechamos a ligacao ao socket,
           retornamos ao main */
        if(strcmp("end", mensagem) == 0){
            // Exibe mensagem quando o cliente se desligou do socket
            printf("\tCliente desligou-se do socket nº: %d\n", cliente_socket);

            // Decrementados o número de clientes conectados
            clientes_ligados--;
            
            // Liberta o espaco em memória para a mensagem
        	free(mensagem);

            // Fechar o ficheiro descritor do socket do cliente
            close(cliente_socket);
            // Encerra a thread
            pthread_exit((void*) 0);
        }

        // Envia para o socket do cliente a mensagem
        escrever_socket(cliente_socket, mensagem);

        // Liberta o espaco em memória para a mensagem
        free(mensagem);
    }
}

/* O servidor vai receber os argumentos para o número máximo de ligações aceites
    e o limite máximo de tempo de inatividade os utilizadores */
int main(int argc, char *argv[]){

    /* Argumentos */
    // Limite máximo de ligações fornecido
    int max_ligacoes = (int)atoi(argv[1]);
    // Limite máximo de tempo de inatividade para os utilizador
    int max_tempo = (int)atoi(argv[2]);

    int valida_entradas;                    // Variável que é enviado ao utilizador

    char* nome_socket = NOME_FICH_SOCKET;   // Nome do socket definido como constante

    int socket_fd;                          // Descritor do socket
    struct sockaddr_un nome;                // Estrutura para o socket unix

    struct sockaddr_un nome_cliente;        // Estrutura para o socket unix do cliente
    socklen_t cliente_nome_len;             // Tamanho do nome do socket do cliente
    int cliente_socket_fd;                  // Descritor do cliente

    pthread_t thread_cliente;               // Identificação das threads

    // Criar um socket local para comunicacao de streams
    socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);

    /* Definir este processo como sendo o servidor,
       preencher a estrutura para o socket */
    nome.sun_family = AF_LOCAL;
    strcpy(nome.sun_path, nome_socket);

    // bind - Vincular o socket com o endereco (nome)
    bind(socket_fd, (struct sockaddr*) &nome, SUN_LEN(&nome));

    /* Configurar o socket para aceitar ligacoes e fica a aguardar por pedidos,
       o primeiro argumento é o decritor de ficheiro para o socket
       o segundo argumento é o máximo de ligações concorrentes
       permitidas para este socket */
    listen(socket_fd, 5);

    printf("\n\tMáximo de conexões definidas em simultâneo: %d\n", max_ligacoes);
    printf("\tMáximo de tempo de inatividade definido: %d\n", max_tempo);
    printf("\n\tServidor em funcionamento e a aguardar conexões...\n");

    // Aceita as ligacoes e recebe as mensagens
    while(1){

        // Aceitar a ligacao e retorna um descritor para o socket cliente
        cliente_socket_fd = accept(socket_fd, (struct sockaddr *) &nome_cliente, &cliente_nome_len);

        // Se o valor do descritor do socket for maior que 0
        if(cliente_socket_fd > 0){

            /* Se o nº de clientes ligados for menor que o valor de ligações em simultâneo
               permite consulta */
            if(clientes_ligados < max_ligacoes){

                // Exibe a mensagem a que socket o cliente se conectou
                printf("\tCliente ligou-se ao socket nº: %d\n", cliente_socket_fd);

                // Incrementa o nº de clientes ligados
                clientes_ligados++;

                /* Cria a thread que vai executar a função ler_socket com o argumento
                   do socket do cliente (cliente_socket_fd) */
                pthread_create(&thread_cliente, NULL, ler_socket, &cliente_socket_fd);

                // Atribui a valida_entradas o valor 0
                valida_entradas = 0;
                // Envia o valor ao cliente o que permite as consultas
                write(cliente_socket_fd, &valida_entradas, sizeof(valida_entradas));

            } else {    // Se o valor de clientes ligados for igual ao valor de ligações em simultâneo permitidas
                
                // Atribui a valida_entradas o valor 1
	            valida_entradas = 1;
	            // Envia o valor ao cliente o que invalida as consultas
	            write(cliente_socket_fd, &valida_entradas, sizeof(valida_entradas));
				// Fechar o ficheiro descritor do socket do cliente
	            close(cliente_socket_fd);
            }
        }
    }

    return 0;
}
