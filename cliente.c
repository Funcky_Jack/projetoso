/*
 * Ficheiro: cliente.c
 * Autor: José Carlos Gaspar Loureiro
 * nº: 13745
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define NOME_FICH_SOCKET "FICH_SOCKET"  // Constante que vai definir o nome do ficherio de socket

// Funcao para validar datas
int valida_data(char data[]){

    char dia[2], mes[2], ano[4];

    strncpy(dia, data, 2);      // Copia os dois primeiros carateres para nova string
    dia[2] = '\0';              // e coloca terminador

    strncpy(mes, &data[3], 2);  // Copia a partir do terceiro caratere os dois primeiros para nova string
    mes[2] = '\0';              // e coloca terminador

    strncpy(ano, &data[6], 4);  // Copia a partir do sexto caratere os quatro primeiros para nova string
    ano[4] = '\0';              // e coloca terminador

    if(atoi(dia) < 0 || atoi(dia) > 31 || atoi(mes) < 0 || atoi(mes) > 12 || atoi(ano) < 1000 || atoi(ano) >= 2200 || data[2] != '/' || data[5] != '/')
        return 0;   // Data inválida devolve '0' (false)
    else
        return 1;   // Data válida devolve '1' (true)
}

// Funcao para escrever no socket (interpretado como um ficheiro)
void escrever_socket(int socketfd, const char* mensagem){
    // Tamanho da mensagem contando o caracter final '\0'
    int tamanho = strlen(mensagem) + 1;

    // Escrever o numero de bytes da mensagem, incluindo o '\0'
    write(socketfd, &tamanho, sizeof(tamanho));

    // Escrever a mensagem
    write(socketfd, mensagem, tamanho);
}

/* funcao para ler no socket (interpretado como um ficheiro)
   enquanto a mensagem do cliente nao vazia, o cliente vai
   lendo o que esta escrito no socket */
int ler_socket(int servidor_socket){

    // Tamanho reservado para o resultado da mensagem do servidor
    char resultado[800] = "";

    /* Ler o resultado se for vazio fechamos a ligacao, ou seja, retornamos
       ao main com valor 0 */
    if(read(servidor_socket, &resultado, sizeof(resultado)) == 0)
        return 0;

    // Escrever no stdout
    printf("%s\n", resultado);
}

int main(int argc, char *argv[]){

    // Nome do socket definido como constante
    char* nome_socket = NOME_FICH_SOCKET;

    int socket_fd;              // Descritor do socket
    struct sockaddr_un nome;    // Estrutura para o socket do unix
    char mensagem[10];          // Variavél que vai conter os valores digitados
    int valida_ligacao;         // Variável de controlo que o valor é atribuido pelo servidor

    // Ciclo infinito
    while(1){

        printf("\n\n\tPara iniciar a ligacao ao servidor digite 'start' ");
        scanf("%s", mensagem);

        // Se for digitado 'start'
        if(strcmp("start", mensagem) == 0){

            // Limpa ecra
            system("clear");

            // Criar o socket
            socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);

            // Estabelecer o nome para o servidor
            nome.sun_family = AF_LOCAL;
            strcpy(nome.sun_path, nome_socket);

            /* Estabelecer o endereco (nome) a ligacao ao socket
               o cliente faz o pedido de ligacao ao socket servidor */
            connect(socket_fd, (struct sockaddr*) &nome, SUN_LEN(&nome));

            // Ler do socket e verificar o valor retornado
            read(socket_fd, &valida_ligacao, sizeof(valida_ligacao));
            // Se o valor de ligação for diferente de 1 continua a execução
            if(valida_ligacao != 1){

                do{

                    printf("\n\n\tDigite uma data no formato dd/mm/aaaa,\n\tpara obter noticias para essa data,\n");
                    printf("\tpara terminar digite 'end'.\n");
                    printf("\tDigite a sua opcao: ");
                    scanf("%s", mensagem);              // Introduzir valor para a variável mensagem                    

                    // Se data introduzida for válida ou for digitado 'end' escreve para o socket
                    if(valida_data(mensagem) || strcmp("end", mensagem) == 0){
                        // Escrever a mensagem no socket se data for valida ou for digitado 'end'
                        escrever_socket(socket_fd, mensagem);

                        // Limpa o ecra
                        system("clear");

                        // Ler do o resultado da pesquisa
                        ler_socket(socket_fd);

                    } else {    // Se não for introduzida uma data válida
                        // Limpa o ecra
                        system("clear");
                        printf("\tData inválida ou comando inválido...\n");
                    }

                }while(strcmp("end", mensagem) != 0);   // Executa ciclo enquanto não for digitado 'end'

                // Fechar o descritor do socket
                close(socket_fd);

            } else {    // Se o valor devolvido do servidor for 1
                // Limpa o ecra
                system("clear");
                // Informa o utilizador
	            printf("\n\tServidor sobrecarregado. Tente mais tarde...\n");
	            // Fechar o descritor do socket
                close(socket_fd);
            }
        }
    }
}
